<html>
<head>
 <meta charset="utf-8">
  <title>lab5</title>
  <link rel="stylesheet" href="style.css">
</head>
  <body>

<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}
?>
    <form action="" method="POST">
      <p> Имя <br>  <input name="fio"  <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print         $values['fio']; ?>" /> 
      </p>
      <p> Email <br> <input name="email"  <?php if ($errors['email']) {print 'class="error"';} ?> 
        value="<?php print $values['email']; ?>" /> </p>
      <p >дата рождения<br>
        <input <?php if ($errors['date']) {print 'class="error"';} ?>  type="date" name = "date" value = "<?php         print $values['date']; ?>"">
      </p>
      <p <?php if ($errors['gender']) {print 'class="error"';} ?> >Пол<br>
        <label>М
          <input type="radio" name="gender" value="M"  <?php if($values['gender'] == 'M') print 'checked'; ?> > 
        </label> 
        <label>Ж
          <input type="radio" name="gender" value="W" <?php if($values['gender'] == 'W') print 'checked'; ?>> 
        </label>
      </p>
      <p <?php if($errors['limb']) print 'class="error"'; ?>>Количество конечностей<br>
          <label>1
            <input type="radio" name="limb" value="1" 
            <?php if($values['limb'] == '1') print 'checked'; ?>>
          </label> 
          <label>2<input type="radio" name="limb" value="2" 
            <?php if($values['limb'] == '2') print 'checked'; ?>>
          </label>
          <label>3
            <input type="radio" name="limb" value="3" 
            <?php if($values['limb'] == '3') print 'checked';?>>
          </label>
          <label>4
            <input type="radio" name="limb" value="4" <?php if($values['limb'] == '4') print 'checked'; ?>>
          </label>
      </p>

      <p>Сверхспособности<br>
        <select name = "abilities[]" multiple <?php if($errors['abilities']) print 'class="error"';  ?>>
          <option value="Immortal"
            <?php   
                if(!empty($values['abilities'])){
                if(in_array('Immortal', json_decode($values['abilities'])))
                print 'selected';          
                }
            ?> 
            >Бессмертие 
          </option>
          <option value="Levitation" 
            <?php   
                if(!empty($values['abilities'])){
                if(in_array('Levitation', json_decode($values['abilities'])))
                print 'selected';          
                }
            ?>  
            >Левитация
          </option> 
          <option  value="Walk through walls"
            <?php   
                if(!empty($values['abilities'])){
                if(in_array('Walk through walls', json_decode($values['abilities'])))
                print 'selected';          
                }
            ?>  
            >Прохождение сквозь стены
           </option> 
         </select>
       </p> 
       <p>Биография<br>
        <textarea <?php if($errors['bio']) print 'class="error"'; ?> name="bio"><?php print $values['bio']; ?></textarea>
       </p>
       <p <?php if($errors['contract']) print 'class="error"'; ?>>С контрактом ознакомлен 
         <input  type="checkbox" name = "contract" <?php if(!empty($values['contract'])) print 'checked';?>>
       </p>  
	     <p> <button type="submit"  value="ok"> Отправить </button> </p>
    </form>
  </body>
</html>
